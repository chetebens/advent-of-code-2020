
def runit():
    src = open('day02.txt', 'r')
    lines = src.readlines()

    ocBottom = []
    ocTop = []
    target = []
    candidate = []

    valid = 0

    for line in lines:
        ocBottom.append(int(line.split(" ")[0].split('-')[0]))
        ocTop.append(int(line.split(" ")[0].split('-')[1]))
        target.append(line.split(" ")[1].split(":")[0])
        candidate.append(line.split(" ")[2])

    for i in range(len(candidate)):
        c = candidate[i].count(target[i])
        if(c >= int(ocBottom[i]) and c <= int(ocTop[i])):

            valid = valid + 1
    return valid

print("Day 02 part 01 Result:")
print(runit())
print("\n")

