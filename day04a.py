year = 2020
day = 4
part = 'a'


def getD():
    d = {
        "byr" : "",
        "iyr" : "",
        "eyr" : "",
        "hgt" : "",
        "hcl" : "",
        "ecl" : "",
        "pid" : "",
        "cid" : ""
    }
    return d


# byr (Birth Year) - four digits; at least 1920 and at most 2002.
# iyr (Issue Year) - four digits; at least 2010 and at most 2020.
# eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
# hgt (Height) - a number followed by either cm or in:
#
#     If cm, the number must be at least 150 and at most 193.
#     If in, the number must be at least 59 and at most 76.
#
# hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
# ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
# pid (Passport ID) - a nine-digit number, including leading zeroes.


def checkStatus(a):
    valid = True
    if (a["byr"] == ""):
        valid = False
    if (a["iyr"] == ""):
        valid = False
    if (a["eyr"] == ""):
        valid = False
    if (a["hgt"] == ""):
        valid = False
    if (a["hcl"] == ""):
        valid = False
    if (a["ecl"] == ""):
        valid = False
    if (a["pid"] == ""):
        valid = False
    # if (a["cid"] == ""):
    #     valid = False
    return valid


def runit():
    src = open('day%s.txt' % (str.zfill(dayString, 2)), 'r')
    lines = src.readlines()
    valid = 0
    a = getD()
    for l in lines:
        for o in l.split(" "):
            if o.split(":")[0] == "\n":
                status = checkStatus(a)
                print("%s  //// %s" % (a, status))
                if(status):
                    valid = valid + 1
                a = getD()
            else:
                value = o.split(":")[1].replace("\n", "")
                a[o.split(":")[0]] = value if len(value) > 0 else ""
    return valid

dayString = str(day)
results = runit()

print("Day %s  part %s Result: %s" % (str.zfill(dayString, 2), part, results))
print("\n")

