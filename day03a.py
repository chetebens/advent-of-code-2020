import subprocess
import os
import numpy
year = 2020
day = 3
part = 'a'

def checkForData(year, day, part):
    filename = 'day%s%s.txt' % (str.zfill(day,2), part)
    if(not os.path.exists(filename)):
        url = "https://adventofcode.com/%s/day/%s/input" % (year, day)
        subprocess.Popen(['curl', url, '.'])


def runit():
    src = open('day%s.txt' % (str.zfill(dayString, 2)), 'r')
    lines = src.readlines()
    length = len(lines[0].strip('\n'))
    height = len(lines)
    array = []
    for l in lines:
        ll = list(l)
        ll.pop()
        array.append(ll)
    x=0

    addx = 3

    running = True
    i = 0
    trees = 0

    output = []

    print(array[0])
    array.pop(0)
    for a in array:
        x = x + addx
        if(x >= len(a)):
            x = x - len(a)
        if(a[x]=="#"):
            a[x] = "X"
            trees = trees + 1
        else:
            a[x] = "O"
        print(a)

    return trees

dayString = str(day)
# checkForData(year, dayString, part)
print("Day %s  part %s Result:" % (str.zfill(dayString, 2), part))
print(runit())
print("\n")

