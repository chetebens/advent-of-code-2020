import re

year = 2020
day = 4
part = 'a'


def getD():
    d = {
        "byr" : "",
        "iyr" : "",
        "eyr" : "",
        "hgt" : "",
        "hcl" : "",
        "ecl" : "",
        "pid" : "",
        "cid" : ""
    }
    return d
failByr = []
failIyr = []
failEyr = []
failHgt = []
failHcl = []
failEcl = []
failPid = []
failed = []

def checkStatus(a, i):
    valid = True
    results = ""
    # byr (Birth Year) - four digits; at least 1920 and at most 2002.
    if(not re.match("^(19[2-9][0-9]|200[0-2])$", a["byr"])):
        valid = False

    # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    if (not re.match("^(201[0-9]|2020)$", a["iyr"])):
        valid = False

    # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    if (not re.match("^(202[0-9]|2030)$", a["eyr"])):
        valid = False

    # hgt (Height) - a number followed by either cm or in:
    if (not re.match("(.*)(in|cm)$", a["hgt"])):
        print("failling units " + a["hgt"])
        valid = False

    #     If cm, the number must be at least 150 and at most 193.
    if (re.match("(.*)cm$", a["hgt"])):
        if(not re.match("^(1[5-8][0-9]|19[0-3])cm$",a["hgt"])):
            print("failling centimeters " + a["hgt"])
            valid = False

    #     If in, the number must be at least 59 and at most 76.
    if (re.match("(.*)in$", a["hgt"])):
        if(not re.match("^(59|6[0-9]|7[0-6])in$",a["hgt"])):
            print("failling inches " + a["hgt"])
            valid = False

    # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    if (not re.match(r'^#(?:[0-9a-fA-F]{6})$', a["hcl"])):
        print("failing Hair Color " + a["hcl"])
        valid = False

    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    eyecolors = ['amb','blu','bnr','gry','hzl','oth']
    if not a["ecl"] in eyecolors:
        print("failling eye color " + a["ecl"])
        valid = False

    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    if (not re.match("^([0-9]{9})$", a["pid"])):
        print("failing on PID " + a["pid"])
        valid = False

    return valid


def runit():
    src = open('day%s.txt' % (str.zfill(dayString, 2)), 'r')
    lines = src.readlines()
    valid = 0
    a = getD()
    finals= []
    j = 0
    c = open("day04testing.txt", "a")

    c.write("byr|iyr|eyr|hgt|hcl|ecl|pid\n")
    for l in lines:
        for o in l.split(" "):
            if o.split(":")[0] == "\n":
                status = checkStatus(a, j)
                j=j+1
                if(status):
                    valid = valid + 1
                    c.write("%s|%s|%s|%s|%s|%s|%s\n" % (a["byr"], a["iyr"], a["eyr"], a["hgt"], a["hcl"], a["ecl"], a["pid"]))
                a = getD()
            else:
                value = o.split(":")[1].replace("\n", "")
                a[o.split(":")[0]] = value if len(value) > 0 else ""
        i = 0

    c.close()

    return valid

dayString = str(day)
results = runit()

print("Day %s  part %s Result: %s" % (str.zfill(dayString, 2), part, results))
print("\n")

