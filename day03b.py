import subprocess
import os
import numpy
year = 2020
day = 3
part = 'a'

def checkForData(year, day, part):
    filename = 'day%s%s.txt' % (str.zfill(day,2), part)
    if(not os.path.exists(filename)):
        url = "https://adventofcode.com/%s/day/%s/input" % (year, day)
        subprocess.Popen(['curl', url, '.'])


def runit(addx, addy):
    src = open('day%s.txt' % (str.zfill(dayString, 2)), 'r')
    lines = src.readlines()
    length = len(lines[0].strip('\n'))
    height = len(lines)
    array = []
    for l in lines:
        ll = list(l)
        ll.pop()
        array.append(ll)
    x=0
    trees = 0

    output = []
    y= addy

    for a in array:
        if y == 0:
            y = addy
            x = x + addx
            if(x >= len(a)):
                x = x - len(a)
            if(a[x]=="#"):
                a[x] = "X"
                trees = trees + 1
            else:
                a[x] = "O"
            y = addy
        y = y - 1
        print(a)


    return trees

dayString = str(day)
# checkForData(year, dayString, part)

ra = []
ra.append(runit(1,1))
ra.append(runit(3,1))
ra.append(runit(5,1))
ra.append(runit(7,1))
ra.append(runit(1,2))

results = 1
for r in ra:
    results = results * r

print("Day %s  part %s Result: %s" % (str.zfill(dayString, 2), part, results))
print("\n")

