import csv
import sys

def checkSum(a, b, c):
    if(a + b == c):
        return True
    return False

def checkSum3(a, b, c, d):
    if(a + b + c == d):
        return True
    return False

def runit():
    src = open('day01.txt', 'r')
    lines = src.readlines()

    count = 0
    listA = []
    # Strips the newline character
    for line in lines:
        listA.append(line)
    listC = listB = listA

    for a in listA:
        for b in listB:
            for c in listC:
                if(checkSum3(int(a),int(b),int(c),2020)):
                    return int(a) * int(b) * int(c)
    return ""


print("Day 01 part 01 Result:")
print(runit())
print("\n")

