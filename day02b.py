
def runit():
    src = open('day02.txt', 'r')
    lines = src.readlines()

    ocBottom = []
    ocTop = []
    target = []
    candidate = []

    valid = 0

    for line in lines:
        ocBottom.append(int(line.split(" ")[0].split('-')[0]))
        ocTop.append(int(line.split(" ")[0].split('-')[1]))
        target.append(line.split(" ")[1].split(":")[0])
        candidate.append(line.split(" ")[2])

    for i in range(len(candidate)):
        c = candidate[i]
        counter = 0
        if(c[ocBottom[i]-1] == target[i]):
            counter = counter + 1
        if (c[ocTop[i]-1] == target[i]):
            counter = counter + 1
        if(counter == 1):
            valid = valid + 1
    return valid

print("Day 02 part 02 Result:")
print(runit())
print("\n")

